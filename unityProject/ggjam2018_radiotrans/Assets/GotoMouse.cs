﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotoMouse : MonoBehaviour {

    public float speed = 150f;
    private Vector3 target;
    private float i = 0f;

    void Start()
    {
        target = transform.position;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            target.z = transform.position.z;
        }
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        i++;
        if (i >= 100){
            transform.position = new Vector3(0,0,0);
            i = 0;
        }
    }
}
